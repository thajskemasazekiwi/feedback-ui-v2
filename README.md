# Kiwi Feedback UI v2

Angular application for gathering feedback from customers of [Thai Massage Kiwi](https://www.thajskemasazekiwi.cz/en/).
It features multiple screens with different single-choice and multiple-choice answers.
Answers are loaded dynamically from JSON files. 
Translated to English and Czech.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You need to have [Node.js](https://nodejs.org/en/) installed.
To be able to submit answers you need to have a working instance of [Kiwi Feedback API](https://gitlab.com/thajskemasazekiwi/feedback-api) or a mock API.

### Installing

Navigate to a desired folder and clone the repository:

```
git clone https://gitlab.com/thajskemasazekiwi/feedback-ui-v2.git
```

Install dependencies with `npm`:

```
npm install
```

Run development environment:

```
npm start
```

Navigate to `http://localhost:4200/` and check out the app.

## Running the tests

### Unit tests

```
ng test --no-watch
```

Unit tests are run for every commit using [Gitlab CI/CD](.gitlab-ci.yml).

### E2E tests

End-to-end Protractor tests are not implemented.

## Deployment

This project relies on [Gitlab CI/CD](.gitlab-ci.yml) for deployment.
Every tagged commit will be automatically deployed to production.

## Documentation

Documentation is [published using Gitlab pages](https://thajskemasazekiwi.gitlab.io/feedback-ui-v2/).

## Built With

* [Angular CLI](https://github.com/angular/angular-cli) - All kinds of Angular agenda.
* [Compodoc](https://compodoc.app/) - Used to generate documentation.
* [ngx-translate](https://github.com/ngx-translate/core) - Internationalization.
* [Angular Material](https://material.angular.io/) - Snackbar component and CDK stepper.

## Authors

* **Jan Sequens** - *Initial work*
