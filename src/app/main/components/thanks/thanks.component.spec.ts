import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ThanksComponent} from './thanks.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslatePipeMock} from '../../../shared/test-utils/translate.pipe.mock';
import {TranslateService} from '@ngx-translate/core';
import {translateServiceStub} from '../../../shared/test-utils/translate.service.stub';
import {SharedService} from '../../../shared/services/shared.service';
import {sharedServiceStub} from '../../../shared/test-utils/shared.service.stub';
import {ActivatedRoute, Router} from '@angular/router';
import {of} from 'rxjs';
import {routerStub} from '../../../shared/test-utils/router.stub';

describe('ThanksComponent', () => {
  let component: ThanksComponent;
  let fixture: ComponentFixture<ThanksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThanksComponent, TranslatePipeMock],
      providers: [
        {provide: SharedService, useValue: sharedServiceStub},
        {provide: TranslateService, useValue: translateServiceStub},
        {provide: Router, useValue: routerStub},
        {
          provide: ActivatedRoute, useValue: {
            queryParamMap: of({
              get() {
                return 1;
              }
            })
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThanksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set happy to true', () => {
    component.ngOnInit();
    expect(component.happyClient).toEqual(true);
  });
});
