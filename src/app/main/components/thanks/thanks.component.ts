import {Component, OnInit} from '@angular/core';
import {SharedService} from '../../../shared/services/shared.service';
import {TranslateService} from '@ngx-translate/core';
import {environment} from '../../../../environments/environment';
import {ActivatedRoute, Router} from '@angular/router';

/**
 * Shows a thank you message after filling out the feedback.
 */
@Component({
  selector: 'app-thanks',
  templateUrl: './thanks.component.html',
  styleUrls: ['./thanks.component.scss']
})
export class ThanksComponent implements OnInit {

  /**
   * Specifies a CZK amount as a parameter of a translated text in the template.
   */
  reviewBonusTranslateParam = {value: environment.other.reviewBonus};

  /**
   * Indicates the client was happy with the experience.
   *
   * If true, suggestion to write a review is displayed.
   */
  happyClient: boolean;

  /**
   * Constructor with injected services.
   * @param sharedService A service shared across the application.
   * @param translate Translation service.
   * @param router Angular router.
   * @param route Service to retrieve parameters for the route.
   */
  constructor(
    private sharedService: SharedService,
    private translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    // set app title once
    this.setTitle();

    // need to subscribe to lang changes manually if translating outside HTML template
    translate.onLangChange.subscribe(
      () => {
        this.setTitle();
      }
    );
  }

  /**
   * Sets app title for this page.
   */
  private setTitle() {
    // translate - uses Observable because it loads data from static file
    this.translate.get('common.thanks.title').subscribe((data: string) => {
      this.sharedService.setScreenTitle(data);
    });
  }

  /**
   * Called after Angular has initialized all data-bound properties.
   *
   * Retrieves the `happy` query parameter.
   */
  ngOnInit(): void {
    // Angular way how to retrieve route params (or query params) safely so they get
    // updated every time, even if the component is re-used when changing only
    // the route param, without first routing to some completely different route.
    // See https://angular.io/guide/router#route-parameters
    // Piping it through SwitchMap cancels a HTTP request as a new one comes in
    // which is a performance-safe approach; however, we don't do any HTTP requests
    // with the params, so we don't need SwitchMap.
    this.route.queryParamMap.subscribe(params => {
      this.happyClient = (+params.get('happy') === 1);
      if (+params.get('redirect') === 1) {
        window.setTimeout(() => {
          this.router.navigate(['/']);
        }, environment.delay.restartAfterCompletedMs);
      }
    });
  }

}
