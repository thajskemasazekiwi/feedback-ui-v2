import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {QuestionStepperComponent} from './question-stepper.component';
import {TranslateService} from '@ngx-translate/core';
import {translateServiceStub} from '../../../shared/test-utils/translate.service.stub';
import {Component, NO_ERRORS_SCHEMA} from '@angular/core';
import {ResultsService} from '../../../shared/services/results.service';
import {resultsServiceStub} from '../../../shared/test-utils/results.service.stub';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {routerStub} from '../../../shared/test-utils/router.stub';
import {Router} from '@angular/router';

// noinspection AngularMissingOrInvalidDeclarationInModule
@Component({selector: 'app-custom-stepper', template: ''})
class CustomStepperStubComponent {
  selected = {
    screen: {
      setTitle() {
      }
    }
  };
}

describe('QuestionStepperComponent', () => {
  let component: QuestionStepperComponent;
  let fixture: ComponentFixture<QuestionStepperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatSnackBarModule],
      declarations: [QuestionStepperComponent, CustomStepperStubComponent],
      providers: [
        {provide: TranslateService, useValue: translateServiceStub},
        {provide: ResultsService, useValue: resultsServiceStub},
        {provide: Router, useValue: routerStub}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
