import {AfterViewChecked, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {StepperSelectionEvent} from '@angular/cdk/stepper';
import {CustomStepComponent} from '../../../shared/components/custom-step/custom-step.component';
import {ResultsService} from '../../../shared/services/results.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';

/**
 * Navigates through questions (screens) using a custom stepper.
 */
@Component({
  selector: 'app-question-stepper',
  templateUrl: './question-stepper.component.html',
  styleUrls: ['./question-stepper.component.scss']
})
export class QuestionStepperComponent implements AfterViewChecked, OnInit {

  /**
   * Stepper component that runs the feedback app workflow.
   *
   * `@ViewChild` decorator grabs a DOM element that matches the selector in the template and saves it to a variable.
   */
  @ViewChild('stepper', {static: false}) stepper;

  /**
   * Reference to a timer that automatically restarts the form after a configured timeout.
   */
  private timeoutHandler: number;

  /**
   * Constructor with injected services.
   *
   * Loads translated erorr message.
   *
   * @param cdRef Change detector used to run change detection manually in `ngAfterViewChecked()`.
   * @param resultsService Service to store user input.
   * @param snackBar Material design notifications.
   * @param translate Translation service.
   * @param router Angular router.
   */
  constructor(
    private cdRef: ChangeDetectorRef,
    private resultsService: ResultsService,
    private snackBar: MatSnackBar,
    private translate: TranslateService,
    private router: Router
  ) {
  }

  // REFACTOR set complete=false and event binding for steps programmatically?

  /**
   * Manipulates the stepper. Goes to the next step.
   */
  stepComplete(goToNextStep: boolean = true) {
    this.stepper.selected.completed = true;
    if (goToNextStep) {
      setTimeout(() => {
        this.stepper.next();
      }, environment.delay.questionAnsweredMs);
    }
  }

  /**
   * Manipulates the stepper. Marks currently selected step as incomplete.
   */
  stepIncomplete() {
    this.stepper.selected.completed = false;
  }

  /**
   * Runs after change detection cycles are done.
   *
   * Sets the screen title for the first time. After this the title will be changed on every stepper change.
   */
  ngAfterViewChecked(): void {
    this.stepper.selected.screen.setTitle();

    // need to run change detection again manually
    this.cdRef.detectChanges();
  }

  /**
   * Do what's needed when stepper step has changed.
   *
   * Asks currently selected screen to change the main app heading.
   * Screen will call a service which will emit a new observable.
   * Also, restart the inactivity timer.
   *
   * @param event Represents changing selected step in the stepper.
   */
  handleStepChange(event: StepperSelectionEvent) {
    // let current screen change main app title
    const step = event.selectedStep as CustomStepComponent;
    step.screen.setTitle();

    // indicate activity
    this.restartInactivityTimeout();
  }

  /**
   * Submits the whole feedback form via a remote API.
   */
  submitFeedback() {
    this.resultsService.saveResults().subscribe(
      () => {
        // "result: success" is actually returned every time by the API if HTTP status was 2xx
        const happyClient = this.resultsService.isClientHappy() ? 1 : 0;
        this.router.navigate(['/thanks'], {queryParams: {happy: happyClient, redirect: 1}});
      },
      () => { // HTTP status != 2xx OR network error
        this.translate.get('common.notifications.network').subscribe((data: string) => {
          this.showNotification(data);
        });
      });
  }

  /**
   * Shows a notification with an error message.
   * @param message Text to show.
   * @param autoHide If true, it'll be dismissed automatically after a delay.
   */
  showNotification(message: string, autoHide: boolean = false) {
    let config = {};
    if (autoHide) {
      config = {duration: environment.delay.notificationAutoHideMs};
    }
    this.snackBar.open(message, 'OK', config);
  }

  /**
   * Restarts (or starts if it's not running yet) a countdown for the form to be reset.
   */
  private restartInactivityTimeout() {
    const delayMs = environment.delay.restartTimoutMs;

    // no automatic restart if timeout set to zero
    if (delayMs <= 0) {
      return;
    }

    // stop the original countdown
    this.stopTimeout();

    // set up new countdown
    // window.setTimeout() returns number, just setTimeout() (from Node) returns Timer
    this.timeoutHandler = window.setTimeout(() => {
      this.resetForm();
    }, delayMs);
  }

  /**
   * Stops the countdown for the form to be reset.
   */
  private stopTimeout() {
    if (this.timeoutHandler) {
      clearTimeout(this.timeoutHandler);
    }
  }

  /**
   * Shows warning and after and after a few moments resets (reloads) the form.
   */
  resetForm() {
    // heads-up that form is about to be restarted
    this.translate.get('common.notifications.restart').subscribe((data: string) => {
      this.showNotification(data, true);
    });

    // restart in a few moments
    window.setTimeout(() => {
      // easiest way how to reset the stepper, all screens and their response badges is to reload the page
      window.location.reload();
    }, environment.delay.restartCancelTimeoutMs);
  }

  /**
   * Called after Angular has initialized all data-bound properties.
   *
   * Start an inactivity countdown.
   */
  ngOnInit(): void {
    this.restartInactivityTimeout();
  }
}
