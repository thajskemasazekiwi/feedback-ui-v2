import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {ScreensModule} from '../screens/screens.module';
import {QuestionStepperComponent} from './components/question-stepper/question-stepper.component';
import {ThanksComponent} from './components/thanks/thanks.component';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    QuestionStepperComponent,
    ThanksComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ScreensModule,
    RouterModule
  ],
  exports: [
    QuestionStepperComponent,
    ThanksComponent
  ]
})
export class MainModule {
}
