import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Title} from '@angular/platform-browser';

/**
 * Top-level application component.
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  /**
   * Constructor with injected services.
   * @param translate Translation service.
   * @param titleService Angular service to set application title.
   */
  constructor(private translate: TranslateService, private titleService: Title) {
    translate.addLangs(['cs', 'en']);
    translate.setDefaultLang('cs');

    // REFACTOR get rid of repetitive code
    // translate - uses Observable because it loads data from static file
    translate.get('common.appTitleLong').subscribe((data: string) => {
      this.titleService.setTitle(data);
    });

    // need to subscribe to lang changes manually if translating outside HTML template
    translate.onLangChange.subscribe(
      () => {
        // translate - uses Observable because it loads data from static file
        translate.get('common.appTitleLong').subscribe((data: string) => {
          this.titleService.setTitle(data);
        });
      }
    );
  }
}

