import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChooseTherapistComponent} from './components/choose-therapist/choose-therapist.component';
import {SharedModule} from '../shared/shared.module';
import {ChooseMassageComponent} from './components/choose-massage/choose-massage.component';
import {ChooseIntensityComponent} from './components/choose-intensity/choose-intensity.component';
import {RateTherapistComponent} from './components/rate-therapist/rate-therapist.component';
import {RateMassageComponent} from './components/rate-massage/rate-massage.component';
import {ChooseReceptionistComponent} from './components/choose-receptionist/choose-receptionist.component';
import {RateReceptionistComponent} from './components/rate-receptionist/rate-receptionist.component';
import {ChooseClientSourceComponent} from './components/choose-client-source/choose-client-source.component';
import {CustomerNoteComponent} from './components/customer-note/customer-note.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    ChooseTherapistComponent,
    ChooseMassageComponent,
    ChooseIntensityComponent,
    RateTherapistComponent,
    RateMassageComponent,
    ChooseReceptionistComponent,
    RateReceptionistComponent,
    ChooseClientSourceComponent,
    CustomerNoteComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule
  ],
  exports: [
    ChooseTherapistComponent,
    ChooseMassageComponent,
    ChooseIntensityComponent,
    RateTherapistComponent,
    RateMassageComponent,
    ChooseReceptionistComponent,
    RateReceptionistComponent,
    ChooseClientSourceComponent,
    CustomerNoteComponent
  ]
})
export class ScreensModule {
}
