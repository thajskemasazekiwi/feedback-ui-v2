import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {BaseScreenComponent} from './base-screen.component';
import {SharedService} from '../../../shared/services/shared.service';
import {TranslateService} from '@ngx-translate/core';
import {Component} from '@angular/core';

// service stubs for `TestScreenComponent`
const sharedServiceStub: Partial<SharedService> = {};
const translateServiceStub: Partial<TranslateService> = {};

// minimal screen component
@Component({
  selector: 'app-test-screen',
  template: '',
  providers: [
    {provide: SharedService, useValue: sharedServiceStub},
    {provide: TranslateService, useValue: translateServiceStub},
    {provide: BaseScreenComponent, useExisting: TestScreenComponent} // to be able to work as BaseScreenComponent
  ]
})
class TestScreenComponent extends BaseScreenComponent {
  constructor(sharedService: SharedService, translateService: TranslateService) {
    super(sharedService, translateService);
  }

  screenComplete(data?: any): void {
  }

  screenIncomplete(): void {
  }
}

describe('BaseScreenComponent', () => {
  let component: BaseScreenComponent;
  let fixture: ComponentFixture<BaseScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestScreenComponent]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
