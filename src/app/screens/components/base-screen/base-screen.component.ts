import {SharedService} from '../../../shared/services/shared.service';
import {EventEmitter, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {environment} from '../../../../environments/environment';

/**
 * Base for other screen components.
 *
 * No need to have a `@Component` decoration here.
 */
export abstract class BaseScreenComponent { // TODO rename? Remove "component" and move?

  /**
   * URL where images for screens are.
   *
   * Auxiliary variable fed to response badges on each screen.
   */
  readonly imageBase = environment.optionValues.urlBase + environment.optionValues.imgFolder;

  /**
   * Output event to signal user answered this screen.
   *
   * `false` suggests not to advance automatically to a next screen
   * but rather wait for the user to confirm manually using a "next" button.
   */
  @Output() complete = new EventEmitter<null | false>();

  /**
   * Output event to signal user unselected the answer.
   */
  @Output() incomplete = new EventEmitter<null>();

  /**
   * Screen title.
   */
  private title: string;

  /**
   * Constructor with injected services.
   * @param sharedService A service shared across the application.
   * @param translate Translation service.
   */
  protected constructor(
    private sharedService: SharedService,
    private translate: TranslateService
  ) {
  }

  /**
   * Calls a service that emits a new screen title observable.
   */
  setTitle() {
    this.sharedService.setScreenTitle(this.title);
  }

  /**
   * Loads the translated title string and saves it in a member variable.
   *
   * Loads the title in the current language and saves it to `title` variable.
   * Also starts listening for a language change event and loads the title in the newly selected language in such case.
   *
   * @param titleToken Translation token to load the translated title from (e. g. 'screens.someScreen.title')
   */
  loadTranslatedTitle(titleToken: string) {

    // load once
    this.translate.get(titleToken).subscribe((data: string) => {
      this.title = data;
    });

    // need to subscribe to lang changes manually if translating outside HTML template
    this.translate.onLangChange.subscribe(
      () => {
        // translate - uses Observable because it loads data from static file
        this.translate.get(titleToken).subscribe((data: string) => {
          this.title = data;
        });
      }
    );

  }

  /**
   * Calls `screenIncomplete()` if nothing is selected in the response badge group.
   * @param values Current response badge group value.
   */
  valueChanged(values: string[]) {
    if (values.length === 0) {
      this.screenIncomplete();
    }
  }

  /**
   * Is called when screen is successfully completed.
   *
   * Should call `this.complete.emit()`.
   *
   * @param data Data from the screen to be stored.
   */
  abstract screenComplete(data?: any): void;

  /**
   * Is called when screen is back in an incomplete state.
   *
   * Should call `this.incomplete.emit()`.
   */
  abstract screenIncomplete(): void;
}
