import {Component} from '@angular/core';
import {BaseScreenComponent} from '../base-screen/base-screen.component';
import {SharedService} from '../../../shared/services/shared.service';
import {OptionValuesService} from '../../../shared/services/option-values.service';
import {TranslateService} from '@ngx-translate/core';
import {Intensity} from '../../../shared/models/intensity.model';
import {ResultsService} from '../../../shared/services/results.service';

/**
 * Component that lets user pick intensity of their massage.
 *
 * Note component providers - inherited class is used to inject base class.
 */
@Component({
  selector: 'app-choose-intensity',
  templateUrl: './choose-intensity.component.html',
  styleUrls: ['./choose-intensity.component.scss'],
  providers: [{provide: BaseScreenComponent, useExisting: ChooseIntensityComponent}]
})
export class ChooseIntensityComponent extends BaseScreenComponent {

  /**
   * List of available massages
   */
  intensityLevels: Intensity[];

  /**
   * Constructor with injected services.
   * @param sharedService Service shared accross the application (stored in parent class).
   * @param optionValuesService Service to retrieve possible responses for feedback answers.
   * @param resultsService Service to store user input.
   * @param translate Translation service.
   */
  constructor(
    sharedService: SharedService,
    private optionValuesService: OptionValuesService,
    private resultsService: ResultsService,
    translate: TranslateService
  ) {
    // call parent constructor
    super(sharedService, translate);

    // load answers
    this.optionValuesService.getIntensityLevels().subscribe(
      (data: Intensity[]) => this.intensityLevels = [...data] // clone with [...]
    );

    // load translated screen title
    this.loadTranslatedTitle('screens.chooseIntensity.title');
  }

  /**
   * Indicates the screen is successfully completed.
   *
   * Stores the answer and emits an event.
   *
   * @param intensity Picked answer to store.
   */
  screenComplete(intensity: 1 | 2 | 3) {
    this.resultsService.setIntensity(intensity);
    this.complete.emit();
  }

  /**
   * Indicates the screen is (again) not completed.
   *
   * Removes the stored answer and emit an event.
   */
  screenIncomplete() {
    this.resultsService.setIntensity(undefined);
    this.incomplete.emit();
  }
}
