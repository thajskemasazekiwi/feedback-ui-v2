import {Component} from '@angular/core';
import {SharedService} from '../../../shared/services/shared.service';
import {OptionValuesService} from '../../../shared/services/option-values.service';
import {Therapist} from '../../../shared/models/therapist.model';
import {ResultsService} from '../../../shared/services/results.service';
import {BaseScreenComponent} from '../base-screen/base-screen.component';
import {TranslateService} from '@ngx-translate/core';
import {PersonWithImage} from '../../../shared/models/person-with-image.model';

/**
 * Component that lets user pick his/her massage therapist.
 *
 * Note component providers - inherited class is used to inject base class.
 */
@Component({
  selector: 'app-choose-therapist',
  templateUrl: './choose-therapist.component.html',
  styleUrls: ['./choose-therapist.component.scss'],
  providers: [{provide: BaseScreenComponent, useExisting: ChooseTherapistComponent}]
})
export class ChooseTherapistComponent extends BaseScreenComponent {

  /**
   * List of available therapists
   */
  therapists: Therapist[];

  /**
   * Constructor with injected services.
   * @param sharedService Service shared accross the application (stored in parent class).
   * @param optionValuesService Service to retrieve possible responses for feedback answers.
   * @param resultsService Service to store user input.
   * @param translate Translation service.
   */
  constructor(
    sharedService: SharedService,
    private optionValuesService: OptionValuesService,
    private resultsService: ResultsService,
    translate: TranslateService
  ) {
    // call parent constructor
    super(sharedService, translate);

    // load answers
    this.optionValuesService.getTherapists().subscribe(
      (data: Therapist[]) => this.therapists = [...data] // clone with [...]
    );

    // load translated screen title
    this.loadTranslatedTitle('screens.chooseTherapist.title');
  }

  /**
   * Indicates the screen is successfully completed.
   *
   * Stores the answer and emits an event.
   *
   * @param therapistData Name and image file name.
   */
  screenComplete(therapistData: PersonWithImage) {
    // therapistData.imageFileName may be 'undefined'
    this.resultsService.setTherapist(therapistData.name, therapistData.imageFileName);
    this.complete.emit();
  }

  /**
   * Indicates the screen is (again) not completed.
   *
   * Removes the stored answer and emit an event.
   */
  screenIncomplete() {
    this.resultsService.setTherapist(undefined);
    this.incomplete.emit();
  }
}
