import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ChooseTherapistComponent} from './choose-therapist.component';
import {TranslatePipeMock} from '../../../shared/test-utils/translate.pipe.mock';
import {resultsServiceStub} from '../../../shared/test-utils/results.service.stub';
import {SharedService} from '../../../shared/services/shared.service';
import {optionValuesServiceStub} from '../../../shared/test-utils/option-values.service.stub';
import {translateServiceStub} from '../../../shared/test-utils/translate.service.stub';
import {sharedServiceStub} from '../../../shared/test-utils/shared.service.stub';
import {OptionValuesService} from '../../../shared/services/option-values.service';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ResultsService} from '../../../shared/services/results.service';

describe('ChooseTherapistComponent', () => {
  let component: ChooseTherapistComponent;
  let fixture: ComponentFixture<ChooseTherapistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChooseTherapistComponent, TranslatePipeMock],
      providers: [
        {provide: SharedService, useValue: sharedServiceStub},
        {provide: OptionValuesService, useValue: optionValuesServiceStub},
        {provide: ResultsService, useValue: resultsServiceStub},
        {provide: TranslateService, useValue: translateServiceStub}
      ],
      schemas: [NO_ERRORS_SCHEMA] // shallow component testing
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseTherapistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
