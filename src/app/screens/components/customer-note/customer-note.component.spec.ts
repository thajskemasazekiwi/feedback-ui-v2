import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CustomerNoteComponent} from './customer-note.component';
import {optionValuesServiceStub} from '../../../shared/test-utils/option-values.service.stub';
import {resultsServiceStub} from '../../../shared/test-utils/results.service.stub';
import {translateServiceStub} from '../../../shared/test-utils/translate.service.stub';
import {SharedService} from '../../../shared/services/shared.service';
import {sharedServiceStub} from '../../../shared/test-utils/shared.service.stub';
import {OptionValuesService} from '../../../shared/services/option-values.service';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ResultsService} from '../../../shared/services/results.service';
import {TranslatePipeMock} from '../../../shared/test-utils/translate.pipe.mock';
import {ReactiveFormsModule} from '@angular/forms';

describe('CustomerNoteComponent', () => {
  let component: CustomerNoteComponent;
  let fixture: ComponentFixture<CustomerNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [CustomerNoteComponent, TranslatePipeMock],
      providers: [
        {provide: SharedService, useValue: sharedServiceStub},
        {provide: OptionValuesService, useValue: optionValuesServiceStub},
        {provide: ResultsService, useValue: resultsServiceStub},
        {provide: TranslateService, useValue: translateServiceStub}
      ],
      schemas: [NO_ERRORS_SCHEMA] // shallow component testing
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a form which is initially valid', () => {
    expect(component.form.valid).toBeTruthy();
  });

  it('should validate e-mail address', () => {
    const email = component.form.get('email');
    const validEmail = 'arnold@bodybuilding.com';
    const invalidEmail = 'I like pizza';

    email.setValue(validEmail);
    expect(email.valid).toBe(true);

    email.setValue(invalidEmail);
    expect(email.valid).toBe(false);
  });
});
