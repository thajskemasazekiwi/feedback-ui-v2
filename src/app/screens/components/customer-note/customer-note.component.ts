import {Component} from '@angular/core';
import {BaseScreenComponent} from '../base-screen/base-screen.component';
import {SharedService} from '../../../shared/services/shared.service';
import {OptionValuesService} from '../../../shared/services/option-values.service';
import {TranslateService} from '@ngx-translate/core';
import {ResultsService} from '../../../shared/services/results.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

/**
 * Component that lets user leave a message and e-mail address for newsletter subscription.
 *
 * Note component providers - inherited class is used to inject base class.
 */
@Component({
  selector: 'app-customer-note',
  templateUrl: './customer-note.component.html',
  styleUrls: ['./customer-note.component.scss'],
  providers: [{provide: BaseScreenComponent, useExisting: CustomerNoteComponent}]
})
export class CustomerNoteComponent extends BaseScreenComponent {

  /**
   * Form to get note and e-mail for newsletter.
   */
  form: FormGroup;

  /**
   * Constructor with injected services.
   * @param sharedService Service shared accross the application (stored in parent class).
   * @param optionValuesService Service to retrieve possible responses for feedback answers.
   * @param resultsService Service to store user input.
   * @param translate Translation service.
   * @param fb Reactive form builder.
   */
  constructor(
    sharedService: SharedService,
    private optionValuesService: OptionValuesService,
    private resultsService: ResultsService,
    translate: TranslateService,
    fb: FormBuilder
  ) {
    // call parent constructor
    super(sharedService, translate);

    // load translated screen title
    this.loadTranslatedTitle('screens.customerNote.title');

    // construct form
    this.form = fb.group({
      note: [],
      email: [null, [Validators.email]]
    });

    // watch field changes
    this.form.get('note').valueChanges.subscribe(() => {
      this.onNoteChange();
    });
    this.form.get('email').valueChanges.subscribe(() => {
      this.onEmailChange();
    });
  }

  /**
   * Indicates the screen is successfully completed.
   *
   * Emits an event.
   */
  screenComplete(): void {
    this.complete.emit();
  }

  /**
   * Indicates the screen is (again) not completed.
   *
   * Emits an event.
   */
  screenIncomplete(): void {
    this.incomplete.emit();
  }

  /**
   * Saves the note if it's not empty.
   */
  onNoteChange() {
    const note = this.form.get('note');

    // if not empty
    if (note.value && note.value.trim()) {
      // save it
      this.resultsService.setNote(note.value.trim());
    } else {
      // delete the saved note
      this.resultsService.setNote(undefined);
    }
  }

  /**
   * Checks e-mail field validity, saves e-mail and sets the screen to complete or incomplete.
   *
   * Called on `valueChanges` and also on `blur` to handle the first use of the field correctly.
   */
  onEmailChange() {
    const email = this.form.get('email');

    // filling in e-mail for the first time - let's wait for the first blur
    if (email.untouched) {
      return;
    }

    // e-mail filled in at least once at this point

    if (email.valid) {
      // if not empty
      if (email.value.trim()) {
        // save it
        this.resultsService.setEmail(email.value.trim());
      } else {
        // delete the saved e-mail
        this.resultsService.setEmail(undefined);
      }
      // mark screen complete
      this.screenComplete();
    } else {
      // delete the saved e-mail
      this.resultsService.setEmail(undefined);
      // mark screen incomplete
      this.screenIncomplete();
    }
  }
}
