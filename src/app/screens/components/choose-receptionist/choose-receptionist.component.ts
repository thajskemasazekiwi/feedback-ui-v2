import {Component} from '@angular/core';
import {BaseScreenComponent} from '../base-screen/base-screen.component';
import {Receptionist} from '../../../shared/models/receptionist.model';
import {SharedService} from '../../../shared/services/shared.service';
import {OptionValuesService} from '../../../shared/services/option-values.service';
import {ResultsService} from '../../../shared/services/results.service';
import {TranslateService} from '@ngx-translate/core';
import {PersonWithImage} from '../../../shared/models/person-with-image.model';

/**
 * Component that lets user pick the receptionist.
 *
 * Note component providers - inherited class is used to inject base class.
 */
@Component({
  selector: 'app-choose-receptionist',
  templateUrl: './choose-receptionist.component.html',
  styleUrls: ['./choose-receptionist.component.scss'],
  providers: [{provide: BaseScreenComponent, useExisting: ChooseReceptionistComponent}]
})
export class ChooseReceptionistComponent extends BaseScreenComponent {

  /**
   * List of available receptionists
   */
  receptionists: Receptionist[];

  /**
   * Constructor with injected services.
   * @param sharedService Service shared accross the application (stored in parent class).
   * @param optionValuesService Service to retrieve possible responses for feedback answers.
   * @param resultsService Service to store user input.
   * @param translate Translation service.
   */
  constructor(
    sharedService: SharedService,
    private optionValuesService: OptionValuesService,
    private resultsService: ResultsService,
    translate: TranslateService
  ) {
    // call parent constructor
    super(sharedService, translate);

    // load answers
    this.optionValuesService.getReceptionists().subscribe(
      (data: Receptionist[]) => this.receptionists = [...data] // clone with [...]
    );

    // load translated screen title
    this.loadTranslatedTitle('screens.chooseReceptionist.title');
  }

  /**
   * Indicates the screen is successfully completed.
   *
   * Stores the answer and emits an event.
   *
   * @param receptionistData Name and image file name.
   */
  screenComplete(receptionistData: PersonWithImage) {
    // receptionistData.imageFileName may be 'undefined'
    this.resultsService.setReceptionist(receptionistData.name, receptionistData.imageFileName);
    this.complete.emit();
  }

  /**
   * Indicates the screen is (again) not completed.
   *
   * Removes the stored answer and emit an event.
   */
  screenIncomplete() {
    this.resultsService.setReceptionist(undefined);
    this.incomplete.emit();
  }
}
