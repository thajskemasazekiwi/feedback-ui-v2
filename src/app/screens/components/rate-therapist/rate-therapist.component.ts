import {Component} from '@angular/core';
import {BaseScreenComponent} from '../base-screen/base-screen.component';
import {SharedService} from '../../../shared/services/shared.service';
import {OptionValuesService} from '../../../shared/services/option-values.service';
import {TranslateService} from '@ngx-translate/core';
import {Rating} from '../../../shared/models/rating.model';
import {ResultsService} from '../../../shared/services/results.service';

/**
 * Component that lets user rate massage therapist's behavior.
 *
 * Note component providers - inherited class is used to inject base class.
 */
@Component({
  selector: 'app-rate-therapist',
  templateUrl: './rate-therapist.component.html',
  styleUrls: ['./rate-therapist.component.scss'],
  providers: [{provide: BaseScreenComponent, useExisting: RateTherapistComponent}]
})
export class RateTherapistComponent extends BaseScreenComponent {

  /**
   * List of available ratings
   */
  ratings: Rating[];

  /**
   * Image of a therapist to display as a badge overlay when rating the therapist.
   *
   * See `imageFileName` in [Therapist interface]{@link Therapist}.
   */
  therapistImageFileName: string;

  /**
   * Constructor with injected services.
   * @param sharedService Service shared accross the application (stored in parent class).
   * @param optionValuesService Service to retrieve possible responses for feedback answers.
   * @param resultsService Service to store user input.
   * @param translate Translation service.
   */
  constructor(
    sharedService: SharedService,
    private optionValuesService: OptionValuesService,
    private resultsService: ResultsService,
    translate: TranslateService,
  ) {
    // call parent constructor
    super(sharedService, translate);

    // load answers
    this.optionValuesService.getRatings().subscribe(
      (data: Rating[]) => this.ratings = [...data] // clone with [...]
    );

    // load translated screen title
    this.loadTranslatedTitle('screens.rateTherapist.title');

    // load therapist overlay image
    this.resultsService.therapistImageFileName$.subscribe(
      (data: string) => this.therapistImageFileName = data
    );
  }

  /**
   * Indicates the screen is successfully completed.
   *
   * Stores the answer and emits an event.
   *
   * @param rating Picked answer to store.
   */
  screenComplete(rating: 1 | 2 | 3 | 4 | 5) {
    this.resultsService.setTherapistRating(rating);
    this.complete.emit();
  }

  /**
   * Indicates the screen is (again) not completed.
   *
   * Removes the stored answer and emit an event.
   */
  screenIncomplete() {
    this.resultsService.setTherapistRating(undefined);
    this.incomplete.emit();
  }
}
