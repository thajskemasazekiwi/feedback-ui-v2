import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RateTherapistComponent} from './rate-therapist.component';
import {TranslatePipeMock} from '../../../shared/test-utils/translate.pipe.mock';
import {SharedService} from '../../../shared/services/shared.service';
import {sharedServiceStub} from '../../../shared/test-utils/shared.service.stub';
import {OptionValuesService} from '../../../shared/services/option-values.service';
import {optionValuesServiceStub} from '../../../shared/test-utils/option-values.service.stub';
import {ResultsService} from '../../../shared/services/results.service';
import {resultsServiceStub} from '../../../shared/test-utils/results.service.stub';
import {TranslateService} from '@ngx-translate/core';
import {translateServiceStub} from '../../../shared/test-utils/translate.service.stub';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('RateTherapistComponent', () => {
  let component: RateTherapistComponent;
  let fixture: ComponentFixture<RateTherapistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RateTherapistComponent, TranslatePipeMock],
      providers: [
        {provide: SharedService, useValue: sharedServiceStub},
        {provide: OptionValuesService, useValue: optionValuesServiceStub},
        {provide: ResultsService, useValue: resultsServiceStub},
        {provide: TranslateService, useValue: translateServiceStub}
      ],
      schemas: [NO_ERRORS_SCHEMA] // shallow component testing
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateTherapistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
