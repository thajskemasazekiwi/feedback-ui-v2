import {Component} from '@angular/core';
import {BaseScreenComponent} from '../base-screen/base-screen.component';
import {Rating} from '../../../shared/models/rating.model';
import {SharedService} from '../../../shared/services/shared.service';
import {OptionValuesService} from '../../../shared/services/option-values.service';
import {ResultsService} from '../../../shared/services/results.service';
import {TranslateService} from '@ngx-translate/core';

/**
 * Component that lets user rate massage receptionist's behavior.
 *
 * Note component providers - inherited class is used to inject base class.
 */
@Component({
  selector: 'app-rate-receptionist',
  templateUrl: './rate-receptionist.component.html',
  styleUrls: ['./rate-receptionist.component.scss'],
  providers: [{provide: BaseScreenComponent, useExisting: RateReceptionistComponent}]
})
export class RateReceptionistComponent extends BaseScreenComponent {

  /**
   * List of available ratings
   */
  ratings: Rating[];

  /**
   * Image of a receptionist to display as a badge overlay when rating the receptionist.
   *
   * See `imageFileName` in [Receptionist interface]{@link Receptionist}.
   */
  receptionistImageFileName: string;

  /**
   * Constructor with injected services.
   * @param sharedService Service shared accross the application (stored in parent class).
   * @param optionValuesService Service to retrieve possible responses for feedback answers.
   * @param resultsService Service to store user input.
   * @param translate Translation service.
   */
  constructor(
    sharedService: SharedService,
    private optionValuesService: OptionValuesService,
    private resultsService: ResultsService,
    translate: TranslateService
  ) {
    // call parent constructor
    super(sharedService, translate);

    // load answers
    this.optionValuesService.getRatings().subscribe(
      (data: Rating[]) => this.ratings = [...data] // clone with [...]
    );

    // load translated screen title
    this.loadTranslatedTitle('screens.rateReceptionist.title');

    // load receptionist overlay image
    this.resultsService.receptionistImageFileName$.subscribe(
      (data: string) => this.receptionistImageFileName = data
    );
  }

  /**
   * Indicates the screen is successfully completed.
   *
   * Stores the answer and emits an event.
   *
   * @param rating Picked answer to store.
   */
  screenComplete(rating: 1 | 2 | 3 | 4 | 5) {
    this.resultsService.setReceptionistRating(rating);
    this.complete.emit();
  }

  /**
   * Indicates the screen is (again) not completed.
   *
   * Removes the stored answer and emit an event.
   */
  screenIncomplete() {
    this.resultsService.setReceptionistRating(undefined);
    this.incomplete.emit();
  }
}
