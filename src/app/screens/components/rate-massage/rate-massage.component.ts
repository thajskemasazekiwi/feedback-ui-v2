import {Component, ViewChild} from '@angular/core';
import {BaseScreenComponent} from '../base-screen/base-screen.component';
import {MassageProblem} from '../../../shared/models/massage-problem.model';
import {SharedService} from '../../../shared/services/shared.service';
import {OptionValuesService} from '../../../shared/services/option-values.service';
import {ResultsService} from '../../../shared/services/results.service';
import {TranslateService} from '@ngx-translate/core';
import {ResponseBadgeGroupComponent} from '../../../shared/components/response-badge-group/response-badge-group.component';

/**
 * Component that lets user rate the massage by checking problems they experienced.
 *
 * Note component providers - inherited class is used to inject base class.
 */
@Component({
  selector: 'app-rate-massage',
  templateUrl: './rate-massage.component.html',
  styleUrls: ['./rate-massage.component.scss'],
  providers: [{provide: BaseScreenComponent, useExisting: RateMassageComponent}]
})
export class RateMassageComponent extends BaseScreenComponent {

  /**
   * Provides a handle for the response badge group used in the template.
   */
  @ViewChild(ResponseBadgeGroupComponent, { static: false }) badgeGroup: ResponseBadgeGroupComponent;

  /**
   * List of available massages
   */
  problems: MassageProblem[];

  /**
   * Constructor with injected services.
   * @param sharedService Service shared accross the application (stored in parent class).
   * @param optionValuesService Service to retrieve possible responses for feedback answers.
   * @param resultsService Service to store user input.
   * @param translate Translation service.
   */
  constructor(
    sharedService: SharedService,
    private optionValuesService: OptionValuesService,
    private resultsService: ResultsService,
    translate: TranslateService
  ) {
    // call parent constructor
    super(sharedService, translate);

    // load answers
    this.optionValuesService.getMassageProblems().subscribe(
      (data: MassageProblem[]) => this.problems = [...data] // clone with [...]
    );

    // load translated screen title
    this.loadTranslatedTitle('screens.rateMassage.title');
  }

  /**
   * Indicates the screen is successfully completed.
   *
   * Stores the answer and emits an event.
   * Unless only first badge on the screen is selected (which is supposed to be the "no problem" value),
   *   the `complete` event is emitted with `false` to indicate it should not move to next screen yet.
   *
   * @param problemsSelected Array of indicated problems. Empty arrays means no problems.
   */
  screenComplete(problemsSelected: string[]) {
    this.resultsService.setMassageProblems(problemsSelected);
    // if only first badge is selected
    if (problemsSelected.length === 1 && problemsSelected[0] === this.badgeGroup.badges.first.id) {
      // emit `complete`
      this.complete.emit();
    } else {
      // emit `complete` but indicate it should not move to another screen yet
      this.complete.emit(false);
    }
  }

  /**
   * Indicates the screen is (again) not completed.
   *
   * Removes the stored answer and emit an event.
   */
  screenIncomplete() {
    this.resultsService.setMassageProblems(undefined);
    this.incomplete.emit();
  }

  /**
   * Calls `screenIncomplete()` if nothing is selected in the response badge group. Otherwise, it calls `screenComplete()`.
   * @param values Current response badge group value.
   */
  valueChanged(values: string[]) {
    if (values.length === 0) {
      this.screenIncomplete();
    } else {
      this.screenComplete(values);
    }
  }
}
