import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RateMassageComponent} from './rate-massage.component';
import {TranslatePipeMock} from '../../../shared/test-utils/translate.pipe.mock';
import {optionValuesServiceStub} from '../../../shared/test-utils/option-values.service.stub';
import {resultsServiceStub} from '../../../shared/test-utils/results.service.stub';
import {translateServiceStub} from '../../../shared/test-utils/translate.service.stub';
import {SharedService} from '../../../shared/services/shared.service';
import {sharedServiceStub} from '../../../shared/test-utils/shared.service.stub';
import {OptionValuesService} from '../../../shared/services/option-values.service';
import {TranslateService} from '@ngx-translate/core';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ResultsService} from '../../../shared/services/results.service';

describe('RateMassageComponent', () => {
  let component: RateMassageComponent;
  let fixture: ComponentFixture<RateMassageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RateMassageComponent, TranslatePipeMock],
      providers: [
        {provide: SharedService, useValue: sharedServiceStub},
        {provide: OptionValuesService, useValue: optionValuesServiceStub},
        {provide: ResultsService, useValue: resultsServiceStub},
        {provide: TranslateService, useValue: translateServiceStub}
      ],
      schemas: [NO_ERRORS_SCHEMA] // shallow component testing
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RateMassageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
