import {Component} from '@angular/core';
import {BaseScreenComponent} from '../base-screen/base-screen.component';
import {ClientSource} from '../../../shared/models/client-source.model';
import {SharedService} from '../../../shared/services/shared.service';
import {OptionValuesService} from '../../../shared/services/option-values.service';
import {ResultsService} from '../../../shared/services/results.service';
import {TranslateService} from '@ngx-translate/core';

/**
 * Component that lets user pick how they heard about the company.
 *
 * Note component providers - inherited class is used to inject base class.
 */
@Component({
  selector: 'app-choose-client-source',
  templateUrl: './choose-client-source.component.html',
  styleUrls: ['./choose-client-source.component.scss'],
  providers: [{provide: BaseScreenComponent, useExisting: ChooseClientSourceComponent}]
})
export class ChooseClientSourceComponent extends BaseScreenComponent {

  /**
   * List of possible client sources
   */
  clientSources: ClientSource[];

  /**
   * Constructor with injected services.
   * @param sharedService Service shared accross the application (stored in parent class).
   * @param optionValuesService Service to retrieve possible responses for feedback answers.
   * @param resultsService Service to store user input.
   * @param translate Translation service.
   */
  constructor(
    sharedService: SharedService,
    private optionValuesService: OptionValuesService,
    private resultsService: ResultsService,
    translate: TranslateService
  ) {
    // call parent constructor
    super(sharedService, translate);

    // load answers
    this.optionValuesService.getClientSources().subscribe(
      (data: ClientSource[]) => this.clientSources = [...data] // clone with [...]
    );

    // load translated screen title
    this.loadTranslatedTitle('screens.chooseClientSource.title');
  }

  /**
   * Indicates the screen is successfully completed.
   *
   * Stores the answer and emits an event.
   *
   * @param clientSource Picked answer to store.
   */
  screenComplete(clientSource: string) {
    this.resultsService.setClientSource(clientSource);
    this.complete.emit();
  }

  /**
   * Indicates the screen is (again) not completed.
   *
   * Removes the stored answer and emit an event.
   */
  screenIncomplete() {
    this.resultsService.setClientSource(undefined);
    this.incomplete.emit();
  }
}
