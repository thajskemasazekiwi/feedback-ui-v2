import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ChooseClientSourceComponent} from './choose-client-source.component';
import {ResultsService} from '../../../shared/services/results.service';
import {SharedService} from '../../../shared/services/shared.service';
import {TranslateService} from '@ngx-translate/core';
import {OptionValuesService} from '../../../shared/services/option-values.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslatePipeMock} from '../../../shared/test-utils/translate.pipe.mock';
import {resultsServiceStub} from '../../../shared/test-utils/results.service.stub';
import {optionValuesServiceStub} from '../../../shared/test-utils/option-values.service.stub';
import {sharedServiceStub} from '../../../shared/test-utils/shared.service.stub';
import {translateServiceStub} from '../../../shared/test-utils/translate.service.stub';

describe('ChooseClientSourceComponent', () => {
  let component: ChooseClientSourceComponent;
  let fixture: ComponentFixture<ChooseClientSourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChooseClientSourceComponent, TranslatePipeMock],
      providers: [
        {provide: SharedService, useValue: sharedServiceStub},
        {provide: OptionValuesService, useValue: optionValuesServiceStub},
        {provide: ResultsService, useValue: resultsServiceStub},
        {provide: TranslateService, useValue: translateServiceStub}
      ],
      schemas: [NO_ERRORS_SCHEMA] // shallow component testing
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseClientSourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
