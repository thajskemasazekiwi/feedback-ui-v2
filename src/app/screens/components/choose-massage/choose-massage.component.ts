import {Component} from '@angular/core';
import {Massage} from '../../../shared/models/massage.model';
import {SharedService} from '../../../shared/services/shared.service';
import {OptionValuesService} from '../../../shared/services/option-values.service';
import {BaseScreenComponent} from '../base-screen/base-screen.component';
import {TranslateService} from '@ngx-translate/core';
import {ResultsService} from '../../../shared/services/results.service';

/**
 * Component that lets user pick massage they had.
 *
 * Note component providers - inherited class is used to inject base class.
 */
@Component({
  selector: 'app-choose-massage',
  templateUrl: './choose-massage.component.html',
  styleUrls: ['./choose-massage.component.scss'],
  providers: [{provide: BaseScreenComponent, useExisting: ChooseMassageComponent}]
})
export class ChooseMassageComponent extends BaseScreenComponent {

  /**
   * List of available massages
   */
  massages: Massage[];

  /**
   * Constructor with injected services.
   * @param sharedService Service shared accross the application (stored in parent class).
   * @param optionValuesService Service to retrieve possible responses for feedback answers.
   * @param resultsService Service to store user input.
   * @param translate Translation service.
   */
  constructor(
    sharedService: SharedService,
    private optionValuesService: OptionValuesService,
    private resultsService: ResultsService,
    translate: TranslateService
  ) {
    // call parent constructor
    super(sharedService, translate);

    // load answers
    this.optionValuesService.getMassages().subscribe(
      (data: Massage[]) => this.massages = [...data] // clone with [...]
    );

    // load translated screen title
    this.loadTranslatedTitle('screens.chooseMassage.title');
  }

  /**
   * Indicates the screen is successfully completed.
   *
   * Stores the answer and emits an event.
   *
   * @param massage Picked answer to store.
   */
  screenComplete(massage: string) {
    this.resultsService.setMassage(massage);
    this.complete.emit();
  }

  /**
   * Indicates the screen is (again) not completed.
   *
   * Removes the stored answer and emit an event.
   */
  screenIncomplete() {
    this.resultsService.setMassage(undefined);
    this.incomplete.emit();
  }
}
