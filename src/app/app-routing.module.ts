import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {QuestionStepperComponent} from './main/components/question-stepper/question-stepper.component';
import {ThanksComponent} from './main/components/thanks/thanks.component';

const routes: Routes = [
  {path: 'questions', component: QuestionStepperComponent},
  {path: 'thanks', component: ThanksComponent},
  {path: '', redirectTo: '/questions', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
