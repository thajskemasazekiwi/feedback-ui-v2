import {OptionValuesService} from '../services/option-values.service';
import {of} from 'rxjs';

// TODO should return test data
/**
 * Option values service stub. Used in testing only.
 */
export const optionValuesServiceStub: Partial<OptionValuesService> = {
  getTherapists() {
    return of([]);
  },
  getMassages() {
    return of([]);
  },
  getIntensityLevels() {
    return of([]);
  },
  getRatings() {
    return of([]);
  },
  getMassageProblems() {
    return of([]);
  },
  getReceptionists() {
    return of([]);
  },
  getClientSources() {
    return of([]);
  }
};
