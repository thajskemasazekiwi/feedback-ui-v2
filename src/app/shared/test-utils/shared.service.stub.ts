import {SharedService} from '../services/shared.service';

/**
 * Shared service stub. Used in testing only.
 */
export const sharedServiceStub: Partial<SharedService> = {
  setScreenTitle() {
  }
};
