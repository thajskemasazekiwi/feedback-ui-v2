import {NgModule, Pipe, PipeTransform} from '@angular/core';

/**
 * Translate pipe mock. Used in testing only.
 */
@Pipe({name: 'translate'})
export class TranslatePipeMock implements PipeTransform {
  transform(value: string): string {
    return value;
  }
}

/**
 * Useless module to include `TranslatePipeMock` in. Required by Angular compiler.
 */
@NgModule({
  declarations: [TranslatePipeMock]
})
export class MockModule {
}
