import {Router} from '@angular/router';

/**
 *  Router stub. Used in testing only.
 */
export const routerStub: Partial<Router> = {
  navigate(): Promise<boolean> {
    return Promise.resolve(true); // navigation succeeded
  },
  isActive(): boolean {
    return true;
  }
};

