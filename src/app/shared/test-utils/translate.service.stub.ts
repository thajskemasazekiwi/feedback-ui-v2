import {LangChangeEvent, TranslateService} from '@ngx-translate/core';
import {of} from 'rxjs';
import {EventEmitter} from '@angular/core';

/**
 * Translate service stub. Used in testing only.
 */
export const translateServiceStub: Partial<TranslateService> = {
  addLangs() {
  },
  setDefaultLang() {
  },
  get(key: string) {
    return of(key);
  },
  onLangChange: new EventEmitter<LangChangeEvent>()
};
