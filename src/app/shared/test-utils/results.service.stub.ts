import {ResultsService} from '../services/results.service';
import {Observable} from 'rxjs';

/**
 * Results service stub. Used in testing only.
 */
export const resultsServiceStub: Partial<ResultsService> = {
  receptionistImageFileName$: new Observable<string>(),
  therapistImageFileName$: new Observable<string>()
};
