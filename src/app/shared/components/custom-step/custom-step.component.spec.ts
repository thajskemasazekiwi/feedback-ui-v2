import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CustomStepComponent} from './custom-step.component';
import {CdkStepper} from '@angular/cdk/stepper';
import {Component, DebugElement} from '@angular/core';
import {BaseScreenComponent} from '../../../screens/components/base-screen/base-screen.component';
import {By} from '@angular/platform-browser';
import {TranslateService} from '@ngx-translate/core';
import {SharedService} from '../../services/shared.service';

// mock stepper to be injected to `CustomStepComponent`
class TestStepper {
}

// service stubs for `TestScreenComponent`
const sharedServiceStub: Partial<SharedService> = {};
const translateServiceStub: Partial<TranslateService> = {};

// mock screen component - `CustomStepComponent` expect a screen inside
@Component({
  selector: 'app-test-screen',
  template: '',
  providers: [
    {provide: SharedService, useValue: sharedServiceStub},
    {provide: TranslateService, useValue: translateServiceStub},
    {provide: BaseScreenComponent, useExisting: TestScreenComponent} // to be able to work as BaseScreenComponent
  ]
})
class TestScreenComponent extends BaseScreenComponent {
  constructor(sharedService: SharedService, translateService: TranslateService) {
    super(sharedService, translateService);
  }

  screenComplete(data?: any): void {
  }

  screenIncomplete(): void {
  }
}

// `CustomStepComponent` expect a screen inside as a `ContentChild`, thus we need to setup a host component
@Component({
  template: `
    <app-custom-step>
      <app-test-screen></app-test-screen>
    </app-custom-step>
  `
})
class TestHostComponent {
}

describe('CustomStepComponent', () => {
  let hostFixture: ComponentFixture<TestHostComponent>;
  let stepDebugElement: DebugElement;
  let stepComponent: CustomStepComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestHostComponent, CustomStepComponent, TestScreenComponent],
      providers: [
        {provide: CdkStepper, useClass: TestStepper} // `CustomStepComponent` injects a stepper
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    hostFixture = TestBed.createComponent(TestHostComponent);
    stepDebugElement = hostFixture.debugElement.query(By.directive(CustomStepComponent));
    stepComponent = stepDebugElement.componentInstance;

    hostFixture.detectChanges();
  });

  it('should create', () => {
    expect(stepComponent).toBeTruthy();
  });
});
