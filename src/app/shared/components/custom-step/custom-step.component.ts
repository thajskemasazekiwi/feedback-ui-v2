import {AfterContentInit, Component, ContentChild} from '@angular/core';
import {CdkStep, CdkStepper} from '@angular/cdk/stepper';
import {BaseScreenComponent} from '../../../screens/components/base-screen/base-screen.component';

/**
 * Step to be used inside `CustomStepperComponent`. Has a screen inside.
 *
 * Must have one `BaseScreenComponent` inside.
 *
 * Extends `CdkStep`. Note component providers - inherited class is used to inject base class - and the HTML template.
 */
@Component({
  selector: 'app-custom-step',
  templateUrl: './custom-step.component.html',
  styleUrls: ['./custom-step.component.scss'],
  providers: [{provide: CdkStep, useExisting: CustomStepComponent}]
})
export class CustomStepComponent extends CdkStep implements AfterContentInit {

  /**
   * Provides a handle for the screen inside.
   */
  @ContentChild(BaseScreenComponent, {static: false}) screen: BaseScreenComponent;

  /**
   * Constructor that only passes required parameters to its parent constructor.
   * @param stepper Stepper.
   */
  constructor(stepper: CdkStepper) {
    super(stepper);
  }

  /**
   * Invoked after Angular has completed initialization of all content (child elements).
   *
   * Checks a screen (BaseScreenComponent) is inside of this step.
   * @example
   * <app-custom-step>
   *                <some-screen></some-screen>
   * </app-custom-step>
   */
  ngAfterContentInit(): void {
    if (this.screen === undefined) {
      throw new Error(
        'Inside stepper, every step has to have a screen inside.' +
        ' Did you forget to specify component providers for BaseScreenComponent in your screen?'
      );
    }
  }
}
