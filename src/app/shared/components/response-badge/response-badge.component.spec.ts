import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ResponseBadgeComponent} from './response-badge.component';

describe('ResponseBadgeComponent', () => {
  let component: ResponseBadgeComponent;
  let fixture: ComponentFixture<ResponseBadgeComponent>;
  let elBadge: HTMLElement;

  const testBadgeId = 'test_badge';
  const testBadgeTitle = 'Test badge title';
  const testBadgeImage = 'unknown.png';
  const testBadgeImageBase = 'assets/sample-options/img/';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResponseBadgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponseBadgeComponent);
    component = fixture.componentInstance;

    // set requried inputs
    component.id = testBadgeId;
    component.title = testBadgeTitle;
    component.imageFileName = testBadgeImage;
    component.imageBase = testBadgeImageBase;

    fixture.detectChanges();

    elBadge = fixture.nativeElement.querySelector('.response-badge');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change status and emit events when clicked', () => {
    let selectedBadgeId = '';
    let deselectedBadgeId = '';
    component.select.subscribe((badgeId: string) => {
      selectedBadgeId = badgeId;
    });
    component.deselect.subscribe((badgeId: string) => {
      deselectedBadgeId = badgeId;
    });

    expect(component.checked).toBe(false);

    elBadge.click(); // select

    expect(component.checked).toBe(true); // status changed
    expect(selectedBadgeId).toBe(testBadgeId); // event fired

    elBadge.click(); // deselect

    expect(deselectedBadgeId).toBe(testBadgeId);
  });
});
