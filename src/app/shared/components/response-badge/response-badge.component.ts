import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

/**
 * Component to represent a check box or a radio button. Must be used inside `ResponseBadgeGroupComponent`.
 */
@Component({
  selector: 'app-response-badge',
  templateUrl: './response-badge.component.html',
  styleUrls: ['./response-badge.component.scss']
})
export class ResponseBadgeComponent implements OnInit {

  /**
   * Unique ID.
   *
   * Required. String event with this value will be emitted on select/deselect or used by parent response badge group.
   */
  @Input() id: string;

  /**
   * Main title.
   *
   * Required.
   */
  @Input() title: string;

  /**
   * Image that creates the badge.
   *
   * Required. Needs to be present in /src/assets/img/.
   */
  @Input() imageFileName: string;

  /**
   * Path where `imageFileName` and `overlayImageFileName` are.
   *
   * Required.
   * **Must include a trailing slash.**
   * Remote or local.
   *
   * @example `https://www.thajskemasazekiwi.cz/feedback-options/img/`
   * @example `assets/sample-options/img/`
   */
  @Input() imageBase: string;

  /**
   * Subtitle or description.
   */
  @Input() subtitle = '';

  /**
   * Image that's shown in a small overlay circle.
   *
   * Needs to be present in /src/assets/img/.
   */
  @Input() overlayImageFileName = '';


  /**
   * If `true`, badges in the same group are all unchecked when this badge is checked.
   *
   * Effective only if parent `ResponseBadgeGroupComponent` has `[multiSelect]="true"`, ignored otherwise.
   */
  @Input() forceUncheckOfAll = false;

  /**
   * List of `id`s of badges in the same group to uncheck when this badge is checked.
   *
   * Effective only if parent `ResponseBadgeGroupComponent` has `[multiSelect]="true"`, ignored otherwise.
   */
  @Input() forceUncheckOf: string[] = [];

  /**
   * Emits an event with `id` when badge is checked.
   *
   * If `id` is not set, default value of an empty string is used.
   */
  @Output() select: EventEmitter<string> = new EventEmitter<string>();

  /**
   * Emits an event with `id` when badge is unchecked.
   *
   * If `id` is not set, default value of an empty string is used.
   */
  @Output() deselect: EventEmitter<string> = new EventEmitter<string>();

  /**
   * Badge state. For standard use case, use `toggle()` rather than manipulating this property directly.
   */
  checked = false;

  /**
   * Checks required attributes (`Input()` parameters).
   *
   * Only checks the attributes are present (not `undefined`), does not check their value.
   */
  checkRequiredFields() {
    if (this.id === undefined) {
      throw new Error('Attribute \'id\' is required');
    }
    if (this.title === undefined) {
      throw new Error('Attribute \'title\' is required');
    }
    if (this.imageFileName === undefined) {
      throw new Error('Attribute \'imageFileName\' is required');
    }
    if (this.imageBase === undefined) {
      throw new Error('Attribute \'imageBase\' is required');
    }
  }

  /**
   * Called after Angular has initialized all data-bound properties.
   */
  ngOnInit(): void {
    this.checkRequiredFields();
  }

  /**
   * Toggles state (checked or unchecked) and emits events.
   */
  toggle(): void {

    // change state
    this.checked = !this.checked;

    // fire an event
    if (this.checked) {
      this.select.emit(this.id);
    } else {
      this.deselect.emit(this.id);
    }

  }
}
