import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ResponseBadgeGroupComponent} from './response-badge-group.component';
import {Component, DebugElement} from '@angular/core';
import {ResponseBadgeComponent} from '../response-badge/response-badge.component';
import {By} from '@angular/platform-browser';

// noinspection AngularMissingOrInvalidDeclarationInModule
@Component({
  template: `
    <app-response-badge-group>
      <app-response-badge id="test_id_1" title="Test Title 1" imageFileName="unknown.png" imageBase="assets/sample-options/img/">
      </app-response-badge>
      <app-response-badge id="test_id_2" title="Test Title 2" imageFileName="unknown.png" imageBase="assets/sample-options/img/">
      </app-response-badge>
      <app-response-badge id="test_id_3" title="Test Title 3" imageFileName="unknown.png" imageBase="assets/sample-options/img/">
      </app-response-badge>
    </app-response-badge-group>
  `
})
class TestHostComponent {
}

/**
 * This is technically an integration tests since it uses real `ResponseBadgeComponent`.
 * Mocking or stubbing `ResponseBadgeComponent` doesn't make much sense, because the component is not very complicated
 *   and `ResponseBadgeGroupComponent` needs most of its functionality to be properly tested.
 */
describe('ResponseBadgeGroupComponent', () => {
  let hostFixture: ComponentFixture<TestHostComponent>;
  let badgeGroupDebugElement: DebugElement;
  let badgeGroupComponent: ResponseBadgeGroupComponent;
  let badges: ResponseBadgeComponent[];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TestHostComponent, ResponseBadgeGroupComponent, ResponseBadgeComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    hostFixture = TestBed.createComponent(TestHostComponent);
    badgeGroupDebugElement = hostFixture.debugElement.query(By.directive(ResponseBadgeGroupComponent));
    badgeGroupComponent = badgeGroupDebugElement.componentInstance;

    hostFixture.detectChanges();

    badges = badgeGroupComponent.badges.toArray();
  });

  it('should create', () => {
    expect(badgeGroupComponent).toBeTruthy();
  });

  it('should make sure no more than one badge is selected (with default configuration)', () => {
    badges[0].checked = true;
    badges[1].toggle();
    // detectChanges() is not needed, we don't base expectations of UI
    // fakeAsync() is not needed either, bcs toggle() emits event which is by default synchronous (https://angular.io/api/core/EventEmitter)
    expect(badges[1].checked).toBe(true, 'the badge we toggled is not selected'); // this we just selected
    expect(badges[0].checked).toBe(false, 'the other badge was not deselected'); // others were deselected
  });

  it('should uncheck all other badges based on `forceUncheckOfAll` badge attribute if `multiSelect` mode is on', () => {
    badgeGroupComponent.multiSelect = true;
    badges[0].forceUncheckOfAll = true;
    badges[1].checked = true;
    badges[2].checked = true;
    badges[0].toggle();
    expect(badges[0].checked).toBe(true, 'the badge we toggled is not selected');
    expect(badges[1].checked).toBe(false, 'badge 1 was no deselected');
    expect(badges[2].checked).toBe(false, 'badge 2 was no deselected');
  });

  it('should uncheck badges based on `forceUncheckOf` badge attribute if `multiSelect` mode is on', () => {
    badgeGroupComponent.multiSelect = true;
    badges[0].forceUncheckOf = [badges[2].id]; // badge 0 deselects badge 2
    badges[1].checked = true;
    badges[2].checked = true;
    badges[0].toggle();
    expect(badges[0].checked).toBe(true, 'the badge we toggled is not selected');
    expect(badges[1].checked).toBe(true, 'badge 1 was deselected, but it should have stayed selected');
    expect(badges[2].checked).toBe(false, 'badge 2 was not deselected');
  });

  it('should emit correct value', () => {
    badgeGroupComponent.multiSelect = true;
    badges[2].forceUncheckOf = [badges[0].id]; // badge 2 deselects badge 0
    let value: string[] = [];
    badgeGroupComponent.value.subscribe((newValue: string[]) => {
      value = newValue;
    });

    badges[0].toggle();
    expect(value).toEqual([badges[0].id], 'value is not ID od badge 0');

    badges[1].toggle();
    const expectedArrayA = [badges[0].id, badges[1].id].sort();
    expect(value.sort()).toEqual(expectedArrayA, 'value is not an array of IDs of badges 0 and 1');

    badges[2].toggle();
    const expectedArrayB = [badges[1].id, badges[2].id].sort(); // badge 0 was deselected with `forceUncheckOf`
    expect(value.sort()).toEqual(expectedArrayB, 'value is not an array of IDs of badges 1 and 2');
  });
});
