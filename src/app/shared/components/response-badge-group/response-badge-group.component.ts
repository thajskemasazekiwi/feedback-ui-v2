import {AfterContentInit, Component, ContentChildren, EventEmitter, Input, Output, QueryList} from '@angular/core';
import {ResponseBadgeComponent} from '../response-badge/response-badge.component';

/**
 * Component to wrap `ResponseBadgeComponent` with to achieve radio button behavior.
 */
@Component({
  selector: 'app-response-badge-group',
  templateUrl: './response-badge-group.component.html',
  styleUrls: ['./response-badge-group.component.scss']
})
export class ResponseBadgeGroupComponent implements AfterContentInit {

  /**
   * Set of `id`s of selected badges.
   *
   * Can be retrieved as an array by calling `getSelectedIds()`.
   */
  private selectedIds: Set<string> = new Set<string>();

  /**
   * Indicates whether multiple badges can be selected in the group.
   *
   * If `false` (default), only one badge can be selected at a time in one group (radio button behavior).
   * If `true`, multiple badges can be selected.
   *
   * `true` is needed for `forceUncheckOfAll` and `forceUncheckOf` properties of child
   * [ResponseBadgeComponent]{@link ResponseBadgeComponent}
   * to take effect.
   */
  @Input() multiSelect = false;

  /**
   * Event emitted when value of the group (selected badges) is changed.
   *
   * Fires only after all implied deselects
   *   (caused by `multiSelect` being `false` or `forceUncheckOfAll` or `forceUncheckOf` being used) are performed.
   */
  @Output() value: EventEmitter<string[]> = new EventEmitter<string[]>();

  /**
   * Provides a handle for child badges.
   */
  @ContentChildren(ResponseBadgeComponent) badges: QueryList<ResponseBadgeComponent>;

  /**
   * Invoked after Angular has completed initialization of all content (child elements).
   *
   * Adds methods as listeners to any change of every child badge.
   * Used for imitating radio button behavior and emitting an event when all badges are deselected.
   */
  ngAfterContentInit() {
    // @ContentChildren already available here

    // add listeners if badges are defined statically (in the template)
    this.addListenersToAllBadges();

    // add listeners again when badges change (e. g. if badges are created later based on option values from an API)
    this.badges.changes.subscribe(() => {
      this.addListenersToAllBadges();
    });
  }

  /**
   * Adds `select` and `deselect` listeners to all child badges.
   */
  private addListenersToAllBadges() {
    // for every badge
    // toArray() should not be needed bcs QueryList should implement Iterable - bug? It is used multiple times in this class.
    for (const badge of this.badges.toArray()) {
      // dynamically add listeners for the `select` output parameter
      this.addSelectListener(badge);

      // dynamically add listeners for the `deselect` output parameter
      this.addDeselectListener(badge);
    }
  }

  /**
   * Subscribes an anonymous listener to the `select` event of the given badge.
   *
   * Adds `id` to `selectedIds` and does some unchecking of other badges if needed.
   *
   * @param badge Badge to subscribes `select` listener to.
   */
  private addSelectListener(badge: ResponseBadgeComponent) {
    badge.select.subscribe(() => {

      // make sure no more than one badge is checked in group if...
      if (
        !this.multiSelect // ... the group is single-select ("radio-button-like")
        || badge.forceUncheckOfAll // ... or this badge has forceUncheckOfAll set to true
      ) {
        // uncheck all others
        this.uncheckOthersInGroup(badge);
        // set selected IDs
        this.selectedIds.clear();
        this.selectedIds.add(badge.id);
        // emit new group value
        this.emitValue();
        // quit
        return;
      }

      // from this point we can assume it's a multi-select group

      // unselect badges listed in forceUncheckOf property
      this.uncheckForced(badge);

      // set selected IDs
      this.selectedIds.add(badge.id);
      // emit new group value
      this.emitValue();
    });
  }

  /**
   * Subscribes an anonymous listener to the `deselect` event of the given badge.
   *
   * Removes `id` from `selectedIds` and calls `emitValue()`.
   *
   * @param badge Badge to subscribes `deselect` listener to.
   */
  private addDeselectListener(badge: ResponseBadgeComponent) {
    badge.deselect.subscribe(() => {

      // remove from selectedIds
      this.selectedIds.delete(badge.id);

      // make parent know if it's an invalid user choice (no selected badge)
      this.emitValue();
    });
  }

  /**
   * Unchecks all badges except for one provided as a parameter. Does not fire `deselect` events.
   * @param checkedBadge Reference to the newly checked badge. Will not be unchecked.
   */
  private uncheckOthersInGroup(checkedBadge: ResponseBadgeComponent) {
    if (checkedBadge.checked) {
      for (const badgeToUncheck of this.badges.toArray()) {
        if (badgeToUncheck !== checkedBadge && badgeToUncheck.checked) {
          badgeToUncheck.checked = false; // uncheck without firing an event
          this.selectedIds.delete(badgeToUncheck.id);
        }
      }
    }
  }

  /**
   * Unchecks badges listed in the `forceUncheckOf` property of the badge provided as parameter.
   * Does not fire `deselect` events.
   * @param checkedBadge Reference to the newly checked badge.
   */
  private uncheckForced(checkedBadge: ResponseBadgeComponent) {
    if (checkedBadge.forceUncheckOf.length > 0) {
      // go through all badges in group and uncheck if needed
      // there is no cascading since forced unchecking can only by caused by selecting (not deselecting)
      for (const badgeToUncheck of this.badges.toArray()) {
        // is this `badgeToUncheck` is on the list to uncheck for this `badge`?
        if (checkedBadge.forceUncheckOf.includes(badgeToUncheck.id) && badgeToUncheck.checked) {
          badgeToUncheck.checked = false; // uncheck without firing an event
          this.selectedIds.delete(badgeToUncheck.id);
        }
      }
    }
  }

  /**
   * Emits `value` with currently selected value(s).
   */
  private emitValue() {
    this.value.emit(this.getSelectedIds());
  }

  /**
   * Gets `id`s of all badges that are selected in this group.
   *
   * Particularly useful with `multiSelect` set to `true`.
   */
  getSelectedIds(): string[] {
    return Array.from(this.selectedIds);
  }
}
