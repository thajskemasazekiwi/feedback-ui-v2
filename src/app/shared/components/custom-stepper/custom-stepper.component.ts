import {Directionality} from '@angular/cdk/bidi';
import {ChangeDetectorRef, Component, EventEmitter, Output} from '@angular/core';
import {CdkStepper} from '@angular/cdk/stepper';

/**
 * Component to create the step-by-step workflow of the feedback form.
 *
 * Based on Angular Material CDK component CdkStepper.
 * This [article on dev.to]{@link https://dev.to/mokkapps/how-i-built-a-custom-stepperwizard-component-using-the-angular-material-cdk-4f34}
 * was used as tutorial to set this up.
 *
 * Note component providers - inherited class is used to inject base class.
 */
@Component({
  selector: 'app-custom-stepper',
  templateUrl: './custom-stepper.component.html',
  styleUrls: ['./custom-stepper.component.scss'],
  providers: [{provide: CdkStepper, useExisting: CustomStepperComponent}]
})
export class CustomStepperComponent extends CdkStepper {

  /**
   * Output event to signal the whole stepper is being finished/submitted.
   */
  @Output() submit = new EventEmitter<null>();

  /**
   * Constructor. Only calls super constructor.
   * @param dir *I don't know how this can be used. However, it's not needed when creating my stepper from the template.*
   * @param changeDetectorRef* I don't know how this can be used. However, it's not needed when creating my stepper from the template.*
   */
  constructor(dir: Directionality, changeDetectorRef: ChangeDetectorRef) {
    // mandatory super constructor call
    super(dir, changeDetectorRef);
  }

  /**
   * Handles clicking the `cdkStepperNext` button. Emits a `submit` event if last step was completed.
   */
  nextClicked() {
    // not the last step or step is incomplete
    if (this.selectedIndex < this.steps.length - 1 || !this.selected.completed) {
      return;
    }
    // submit
    this.submit.emit();
  }

}
