import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {HeaderComponent} from './header.component';
import {TranslateService} from '@ngx-translate/core';
import {SharedService} from '../../services/shared.service';
import {of} from 'rxjs';
import {Router} from '@angular/router';
import {routerStub} from '../../test-utils/router.stub';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let translateServiceStub: Partial<TranslateService>;
  let sharedServiceStub: Partial<SharedService>;
  let elMainTitle: HTMLElement;
  let testScreenTitle: string;
  let elFirstLanguage: HTMLElement;
  let translateService: TranslateService;

  // translate service stub
  translateServiceStub = {
    getLangs() {
      return ['cs', 'en'];
    },
    currentLang: 'cs',
    use() {
      return of([]);
    } // return empty Observable to match the original method signature
  };

  beforeEach(async(() => {

    // shared service stub
    testScreenTitle = 'How are you today?';
    sharedServiceStub = {
      screenTitle$: of(testScreenTitle) // synchronous Observable
    };

    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      providers: [
        {provide: TranslateService, useValue: translateServiceStub},
        {provide: SharedService, useValue: sharedServiceStub},
        {provide: Router, useValue: routerStub}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    translateService = fixture.debugElement.injector.get(TranslateService); // it's safer to use component's injector

    fixture.detectChanges(); // must be called here for the `elFirstLanguage` selector to work

    elMainTitle = fixture.nativeElement.querySelector('h1');
    elFirstLanguage = fixture.nativeElement.querySelector('ul.languages > li'); // selects in elements generated with *ngFor!
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show screen title', () => {
    expect(elMainTitle.textContent).toContain(testScreenTitle);
  });

  it('should change language when clicked', () => {
    const translateServiceSpy = spyOn(translateService, 'use');
    elFirstLanguage.click();
    expect(translateServiceSpy).toHaveBeenCalled();
  });

});
