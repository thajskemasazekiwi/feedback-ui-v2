import {Component} from '@angular/core';
import {SharedService} from '../../services/shared.service';
import {TranslateService} from '@ngx-translate/core';
import {Router} from '@angular/router';

/**
 * Shows page header with a title.
 */
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  /**
   * Screen title observable.
   */
  screenTitle$ = this.sharedService.screenTitle$;

  /**
   * Constructor with injected services.
   * @param sharedService A service shared accross the application.
   * @param translate Translation service.
   * @param router Angular router.
   */
  constructor(
    private sharedService: SharedService,
    private translate: TranslateService,
    public router: Router
  ) {
  }

  /**
   * Gets available translation languages.
   */
  getLangs() {
    return this.translate.getLangs();
  }

  /**
   * Gets currently selected translation language.
   */
  getCurrentLang() {
    return this.translate.currentLang;
  }

  /**
   * Selects translation language.
   * @param lang Language identifier.
   */
  setLang(lang: string) {
    this.translate.use(lang);
  }

  /**
   * Reloads current page.
   */
  reloadPage() {
    window.location.reload();
  }
}
