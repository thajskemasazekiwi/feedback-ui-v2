/**
 * Model to represent data about a massage.
 */
export interface Massage {

  /**
   * Unique identification string.
   *
   * Only lower case letters and underscores.
   */
  id: string;

  /**
   * Title.
   *
   * This is displayed to user.
   */
  title: string;

  /**
   * Description.
   *
   * Subtitle displayed to user.
   */
  description: string;

  /**
   * File name of an image to display.
   *
   * Is either a file name only or a full URL.
   * If it's a file name, it will be looked for in {projectRoot}/src/assets/img.
   * If it's a full URL, it will used as is.
   *
   * Example usage:
   *
   * therapist_joe.jpg // local file
   *
   * https://mydomain.com/therapist_bob.jpg // online file
   */
  imageFileName: string;
}
