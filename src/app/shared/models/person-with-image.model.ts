/**
 * Model to represent data being passed on screens about therapist, receptionists etc.
 */
export interface PersonWithImage {
  /**
   * Name of the person.
   */
  name: string;

  /**
   * Image file name. See {@link Therapist} or {@link Receptionist}.
   */
  imageFileName: string;
}
