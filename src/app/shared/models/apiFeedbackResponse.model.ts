/**
 * Model to represent JSON API response after submitting the feedback.
 */
export interface ApiFeedbackResponse {
  /**
   * Indicates submission success.
   */
  success: boolean;
}
