/**
 * Defines feedback answers.
 */
export interface Feedback {
  /**
   * Name of the therapist.
   */
  therapistName: string;

  /**
   * Name of the massage.
   */
  massageName: string;

  /**
   * Massage intensity.
   *
   * 1 is mild, 2 is medium, 3 is hard.
   */
  massageIntensity: 1 | 2 | 3;

  /**
   * Therapist rating.
   *
   * 1 is worst, 5 is best.
   */
  therapistRating: 1 | 2 | 3 | 4 | 5;

  /**
   * List of problems during massage.
   */
  massageProblems: string[];

  /**
   * Name of the receptionist.
   */
  receptionistName: string;

  /**
   * Receptionist rating.
   *
   * 1 is worst, 5 is best.
   */
  receptionistRating: 1 | 2 | 3 | 4 | 5;

  /**
   * How did the customer find us.
   */
  clientSource: string;

  /**
   * Customer's note.
   */
  note: string;

  /**
   * Customer's e-mail.
   */
  email: string;
}
