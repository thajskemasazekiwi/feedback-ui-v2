import {Injectable} from '@angular/core';
import {Feedback} from '../models/feedback.model';
import {Therapist} from '../models/therapist.model';
import {Observable, Subject, throwError} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {catchError} from 'rxjs/operators';
import {ApiFeedbackResponse} from '../models/apiFeedbackResponse.model';

/**
 * Stores feedback answers.
 */
@Injectable({
  providedIn: 'root'
})
export class ResultsService {

  // FIXME Use different approach. No problem value should not be hardcoded here since it's loaded from an external file.
  /**
   * If `Feedback`'s `massageProblems` field only contains this value, it represents client was completely satisfied.
   */
  static readonly massageNoProblemValue = 'no_problem';

  /**
   * Feedback object with undefined values
   */
  private feedback: Feedback = {} as any;

  /**
   * Observable string source
   */
  private therapistImageFileNameSource = new Subject<string>();

  /**
   * Observable string source
   */
  private receptionistImageFileNameSource = new Subject<string>();

  /**
   * Observable string stream
   *
   * Why to use this? See https://stackoverflow.com/questions/36986548/when-to-use-asobservable-in-rxjs
   */
  therapistImageFileName$ = this.therapistImageFileNameSource.asObservable();

  /**
   * Observable string stream
   *
   * Why to use this? See https://stackoverflow.com/questions/36986548/when-to-use-asobservable-in-rxjs
   */
  receptionistImageFileName$ = this.receptionistImageFileNameSource.asObservable();

  /**
   * Constructor with injected services.
   * @param http HTTP service for API calls.
   */
  constructor(private http: HttpClient) {
  }

  /**
   * Sets therapist's name and broadcasts its image filename.
   *
   * Emits an `Observable` with the name of the image representing the therapist (used on other screens).
   *
   * @param name Name of the therapist.
   * @param imageFileName Image of the therapist. See `imageFileName` in [Therapist interface]{@link Therapist}.
   */
  setTherapist(name: string, imageFileName?: string) {
    this.feedback.therapistName = name;
    // set image if truthy, set undefined otherwise
    this.therapistImageFileNameSource.next(imageFileName ? imageFileName : undefined);
  }

  /**
   * Sets receptionist's name and broadcasts its image filename.
   *
   * Emits an `Observable` with the name of the image representing the receptionist (used on other screens).
   *
   * @param name Name of the receptionist.
   * @param imageFileName Image of the receptionist. See `imageFileName` in [Receptionist interface]{@link Receptionist}.
   */
  setReceptionist(name: string, imageFileName?: string) {
    this.feedback.receptionistName = name;
    // set image if truthy, set undefined otherwise
    this.receptionistImageFileNameSource.next(imageFileName ? imageFileName : undefined);
  }

  /**
   * Sets name of the massage.
   * @param name Name of the massage.
   */
  setMassage(name: string) {
    this.feedback.massageName = name;
  }

  /**
   * Sets intensity of the massage.
   * @param intensity Intensity. 1 is mild, 2 is medium, 3 is hard.
   */
  setIntensity(intensity: 1 | 2 | 3) {
    this.feedback.massageIntensity = intensity;
  }

  /**
   * Sets problems experienced during massage.
   * @param problems Array of problems.
   */
  setMassageProblems(problems: string[]) {
    this.feedback.massageProblems = problems;
  }

  /**
   * Sets massage therapist rating.
   * @param rating 1 is worst, 5 is best.
   */
  setTherapistRating(rating: 1 | 2 | 3 | 4 | 5) {
    this.feedback.therapistRating = rating;
  }

  /**
   * Sets receptionist rating.
   * @param rating 1 is worst, 5 is best.
   */
  setReceptionistRating(rating: 1 | 2 | 3 | 4 | 5) {
    this.feedback.receptionistRating = rating;
  }

  /**
   * Sets client source.
   * @param name Name of the client source.
   */
  setClientSource(name: string) {
    this.feedback.clientSource = name;
  }

  /**
   * Sets a message left by the client.
   * @param note Text of the message.
   */
  setNote(note: string) {
    this.feedback.note = note;
  }

  /**
   * Set client's e-mail address.
   * @param email E-mail address.
   */
  setEmail(email: string) {
    this.feedback.email = email;
  }

  /**
   * Checks that all necessary feedback fields are populated.
   */
  private isFeedbackValid(): boolean {
    const fb = this.feedback;
    // e-mail and note can be undefined
    return fb.therapistName !== undefined &&
      fb.massageName !== undefined &&
      fb.massageIntensity !== undefined &&
      fb.therapistRating !== undefined &&
      fb.massageProblems !== undefined &&
      fb.receptionistName !== undefined &&
      fb.receptionistRating !== undefined &&
      fb.clientSource !== undefined;
  }

  /**
   * Returns true if client choose maximum ratings and indicated no problems with the massage, false otherwise.
   */
  isClientHappy(): boolean {
    const fb = this.feedback;
    // need to convert ratings to a number - why?
    return +fb.therapistRating === 5
      && +fb.receptionistRating === 5
      && fb.massageProblems.length === 1
      && fb.massageProblems[0] === ResultsService.massageNoProblemValue;
  }

  /**
   * Sends the feedback object to an API.
   */
  saveResults(): Observable<ApiFeedbackResponse> {
    if (!this.isFeedbackValid()) {
      console.warn('Results are incomplete, not sending to API.');
      return;
    }

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

    // make the API call and return `Observable` (and pipe it through to the error handler)
    const url = environment.results.urlBase + environment.results.endpoint;
    return this.http.post<ApiFeedbackResponse>(url, this.feedback, httpOptions)
      .pipe(
        // `catchError` catches errors on the observable to be handled by returning a new observable or throwing an error
        catchError(this.handleError)
      );
  }

  /**
   * API error handler that throws a new bad Observable with a message.
   *
   * See https://angular.io/guide/http#error-details .
   *
   * @param error `HttpClient`'s error type
   */
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
}
