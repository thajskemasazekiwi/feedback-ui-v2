import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

/**
 * A service shared across the application for communication between components.
 */
@Injectable({
  providedIn: 'root'
})
export class SharedService {

  /**
   * Observable string source
   */
  private screenTitleSource = new BehaviorSubject<string>('...');

  /**
   * Observable string stream
   *
   * Why to use this? See https://stackoverflow.com/questions/36986548/when-to-use-asobservable-in-rxjs
   */
  screenTitle$ = this.screenTitleSource.asObservable();

  /**
   * Emits screen title.
   * @param title String title to be emitted.
   */
  setScreenTitle(title: string) {
    this.screenTitleSource.next(title);
    // setTimeout(() => this.screenTitleSource.next(title), 500);
  }
}
