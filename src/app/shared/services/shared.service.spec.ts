import {fakeAsync, flush, TestBed} from '@angular/core/testing';

import {SharedService} from './shared.service';

describe('SharedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SharedService = TestBed.get(SharedService);
    expect(service).toBeTruthy();
  });

  it('should have title with initial value', () => {
    const service: SharedService = TestBed.get(SharedService);
    service.screenTitle$.subscribe((title: string) => {
      expect(title).toBeTruthy();
    });
  });

  it('should set title', fakeAsync(() => {
    const service: SharedService = TestBed.get(SharedService);
    const textValue = 'My awesome title';
    service.setScreenTitle(textValue);
    flush();
    service.screenTitle$.subscribe((newTitle: string) => {
      expect(newTitle).toBe(textValue);
    });
  }));

});
