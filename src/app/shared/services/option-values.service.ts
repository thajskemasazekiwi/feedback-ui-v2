import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Receptionist} from '../models/receptionist.model';
import {Therapist} from '../models/therapist.model';
import {Observable} from 'rxjs';
import {Massage} from '../models/massage.model';
import {Intensity} from '../models/intensity.model';
import {Rating} from '../models/rating.model';
import {MassageProblem} from '../models/massage-problem.model';
import {environment} from '../../../environments/environment';
import {ClientSource} from '../models/client-source.model';

/**
 * Service to get options for feedback answers.
 *
 * Retrieves current receptionists, massage therapists etc.
 */
@Injectable({
  providedIn: 'root'
})
export class OptionValuesService {

  /**
   * Returns URL prefix common to all endpoints.
   */
  private static urlPrefix() {
    return environment.optionValues.urlBase;
  }

  /**
   * Constructor with injected services.
   * @param http HTTP service for API calls.
   */
  constructor(private http: HttpClient) {
  }

  /**
   * Returns a list of therapists user can choose from.
   */
  getTherapists(): Observable<Therapist[]> {
    return this.http.get<Therapist[]>(
      OptionValuesService.urlPrefix()
      + environment.optionValues.urls.therapists
    );
  }

  /**
   * Returns a list of massages user can choose from.
   */
  getMassages(): Observable<Massage[]> {
    return this.http.get<Massage[]>(
      OptionValuesService.urlPrefix()
      + environment.optionValues.urls.massages
    );
  }

  /**
   * Returns intensity levels user can choose from.
   */
  getIntensityLevels(): Observable<Intensity[]> {
    return this.http.get<Intensity[]>(
      OptionValuesService.urlPrefix()
      + environment.optionValues.urls.intensityLevels
    );
  }

  /**
   * Returns ratings user can use to rate behavior of staff.
   */
  getRatings(): Observable<Rating[]> {
    return this.http.get<Rating[]>(
      OptionValuesService.urlPrefix()
      + environment.optionValues.urls.ratings
    );
  }

  /**
   * Returns problems user can indicate he was experiencing during a massage.
   */
  getMassageProblems(): Observable<MassageProblem[]> {
    return this.http.get<MassageProblem[]>(
      OptionValuesService.urlPrefix()
      + environment.optionValues.urls.massageProblems
    );
  }

  /**
   * Returns a list of receptionists user can choose from.
   */
  getReceptionists(): Observable<Receptionist[]> {
    return this.http.get<Receptionist[]>(
      OptionValuesService.urlPrefix()
      + environment.optionValues.urls.receptionists
    );
  }

  /**
   * Returns a list of client source user can indicate.
   */
  getClientSources(): Observable<ClientSource[]> {
    return this.http.get<ClientSource[]>(
      OptionValuesService.urlPrefix()
      + environment.optionValues.urls.clientSources
    );
  }
}
