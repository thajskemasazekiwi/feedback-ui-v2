import {TestBed} from '@angular/core/testing';
import {OptionValuesService} from './option-values.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {environment} from '../../../environments/environment';
import {Therapist} from '../models/therapist.model';
import {Massage} from '../models/massage.model';
import {Intensity} from '../models/intensity.model';
import {Rating} from '../models/rating.model';
import {Receptionist} from '../models/receptionist.model';
import {MassageProblem} from '../models/massage-problem.model';
import {ClientSource} from '../models/client-source.model';

describe('OptionValuesService', () => {

  // declare variables
  let service: OptionValuesService;
  let httpTestingController: HttpTestingController;

  // test data
  const testTherapists: Therapist[] = [
    {id: 'marie_fredriksson', title: 'Marie Fredriksson', imageFileName: 'image_of_marie.png'},
    {id: 'per_gessle', title: 'Per Gessle', imageFileName: 'https://somesite.com/per-gessle.png'}
  ];
  const testMassages: Massage[] = [
    {id: 'fingernail_massage', title: 'Fingernail massage', description: 'Forget nail polishing.', imageFileName: 'nails.jpg'},
    {id: 'lip_massage', title: 'Lip massage (upper lip only)', description: 'Lower lip cannot be massaged.', imageFileName: 'lips.jpg'}
  ];
  const testIntensityLevels: Intensity[] = [
    {id: 'strong', title: 'Strong', imageFileName: 'power.jpeg'},
    {id: 'arnold', title: 'Arnold-level', imageFileName: 'https//bodybuilding.com/arnie.png'}
  ];
  const testRatings: Rating[] = [
    {id: '1', title: 'Poor', description: '', imageFileName: 'poor-rating.jpg'},
    {id: '5', title: 'Very good', description: 'Almost excellent.', imageFileName: 'five-stars.jpg'}
  ];
  const testMassageProblems: MassageProblem[] = [
    {id: 'fat_therapist', title: 'Therapist was too large', description: 'I am so sorry.', imageFileName: 'whale.jpg'},
    {id: 'cheesy_music', title: 'Music was just to cheesy', description: '', imageFileName: 'justin.jpg'}
  ];
  const testReceptionists: Receptionist[] = [
    {id: 'pam', title: 'Pam Beesly', imageFileName: 'pamela.jpeg'},
    {id: 'erin', title: 'Erin Hannon', imageFileName: 'https://zombiesruineverything.files.wordpress.com/2013/11/erin.jpg'}
  ];
  const testClientSources: ClientSource[] = [
    {id: 'tv', title: 'Television', description: 'TV commercials.', imageFileName: 'tv.jpg'},
    {id: 'tinder', title: 'Tinder and other dating sites', description: '', imageFileName: 'tinder.jpg'}
  ];

  beforeEach(() => {
    // set up test module with HTTP testing module imported
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });

    // inject the service and test controller (for mock HTTP calls) for each test
    service = TestBed.get(OptionValuesService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    // make sure there are no more pending requests
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return therapists', () => {

    // call the method of my service (HTTP calls will be mocked) and subscribe
    service.getTherapists().subscribe((newTherapists: Therapist[]) => {
      expect(newTherapists).toEqual(testTherapists);
    });

    // get request the pending (mocked) request
    const url = environment.optionValues.urlBase + environment.optionValues.urls.therapists;
    const request = httpTestingController.expectOne({url, method: 'GET'});

    // mock the request and let it return test data
    request.flush(testTherapists);
  });

  it('should return massages', () => {

    // call the method of my service (HTTP calls will be mocked) and subscribe
    service.getMassages().subscribe((newMassages: Massage[]) => {
      expect(newMassages).toEqual(testMassages);
    });

    // get request the pending (mocked) request
    const url = environment.optionValues.urlBase + environment.optionValues.urls.massages;
    const request = httpTestingController.expectOne({url, method: 'GET'});

    // mock the request and let it return test data
    request.flush(testMassages);
  });

  it('should return intensity levels', () => {

    // call the method of my service (HTTP calls will be mocked) and subscribe
    service.getIntensityLevels().subscribe((newLevels: Intensity[]) => {
      expect(newLevels).toEqual(testIntensityLevels);
    });

    // get request the pending (mocked) request
    const url = environment.optionValues.urlBase + environment.optionValues.urls.intensityLevels;
    const request = httpTestingController.expectOne({url, method: 'GET'});

    // mock the request and let it return test data
    request.flush(testIntensityLevels);
  });

  it('should return ratings', () => {

    // call the method of my service (HTTP calls will be mocked) and subscribe
    service.getRatings().subscribe((newRatings: Rating[]) => {
      expect(newRatings).toEqual(testRatings);
    });

    // get request the pending (mocked) request
    const url = environment.optionValues.urlBase + environment.optionValues.urls.ratings;
    const request = httpTestingController.expectOne({url, method: 'GET'});

    // mock the request and let it return test data
    request.flush(testRatings);
  });

  it('should return massage problems', () => {

    // call the method of my service (HTTP calls will be mocked) and subscribe
    service.getMassageProblems().subscribe((newProblems: MassageProblem[]) => {
      expect(newProblems).toEqual(testMassageProblems);
    });

    // get request the pending (mocked) request
    const url = environment.optionValues.urlBase + environment.optionValues.urls.massageProblems;
    const request = httpTestingController.expectOne({url, method: 'GET'});

    // mock the request and let it return test data
    request.flush(testMassageProblems);
  });

  it('should return receptionists', () => {

    // call the method of my service (HTTP calls will be mocked) and subscribe
    service.getReceptionists().subscribe((newReceptionists: Receptionist[]) => {
      expect(newReceptionists).toEqual(testReceptionists);
    });

    // get request the pending (mocked) request
    const url = environment.optionValues.urlBase + environment.optionValues.urls.receptionists;
    const request = httpTestingController.expectOne({url, method: 'GET'});

    // mock the request and let it return test data
    request.flush(testReceptionists);
  });

  it('should return client sources', () => {

    // call the method of my service (HTTP calls will be mocked) and subscribe
    service.getClientSources().subscribe((newSources: ClientSource[]) => {
      expect(newSources).toEqual(testClientSources);
    });

    // get request the pending (mocked) request
    const url = environment.optionValues.urlBase + environment.optionValues.urls.clientSources;
    const request = httpTestingController.expectOne({url, method: 'GET'});

    // mock the request and let it return test data
    request.flush(testClientSources);
  });

});
