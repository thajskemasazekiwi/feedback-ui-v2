import {fakeAsync, flush, TestBed} from '@angular/core/testing';

import {ResultsService} from './results.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('ResultsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule]
  }));

  it('should be created', () => {
    const service: ResultsService = TestBed.get(ResultsService);
    expect(service).toBeTruthy();
  });

  it('emits image file name when therapist is set', fakeAsync(() => {
    const service: ResultsService = TestBed.get(ResultsService);
    const testName = 'Maria';
    const testImageFileName = 'maria_image.jpg';
    service.therapistImageFileName$.subscribe((newImageFileName) => {
      expect(newImageFileName).toBe(testImageFileName);
    });
    service.setTherapist(testName, testImageFileName);
    flush();
  }));
});
