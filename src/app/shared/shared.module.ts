import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './components/header/header.component';
import {ResponseBadgeComponent} from './components/response-badge/response-badge.component';
import {HttpClientModule} from '@angular/common/http';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CustomStepperComponent} from './components/custom-stepper/custom-stepper.component';
import {CustomStepComponent} from './components/custom-step/custom-step.component';
import {TranslateModule} from '@ngx-translate/core';
import {ResponseBadgeGroupComponent} from './components/response-badge-group/response-badge-group.component';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    HeaderComponent,
    ResponseBadgeComponent,
    ResponseBadgeGroupComponent,
    CustomStepperComponent,
    CustomStepComponent,
    ResponseBadgeGroupComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    CdkStepperModule,
    MatSnackBarModule,
    BrowserAnimationsModule,
    TranslateModule,
    RouterModule
  ],
  exports: [
    HeaderComponent,
    ResponseBadgeComponent,
    ResponseBadgeGroupComponent,
    CustomStepperComponent,
    CustomStepComponent,
    MatSnackBarModule,
    BrowserAnimationsModule,
    TranslateModule
  ]
})
export class SharedModule {
}
