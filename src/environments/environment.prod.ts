/**
 * @ignore
 */
export const environment = {
  production: true,
  name: 'prod',

  /**
   * Configuration related to fetching option values (possible answers for questions)
   */
  optionValues: {

    /**
     * Base URL (remote or local folder) where JSON option value files are taken from.
     *
     * **Must include a trailing slash.**
     * Option value files are JSON files and contain answers for screens.
     * Note that requesting an online JSON resource from a different domain will likely fail due to CORS restrictions.
     *
     * @example `https://www.thajskemasazekiwi.cz/feedback-options/`
     * @example `assets/sample-options/`
     */
    urlBase: 'https://www.thajskemasazekiwi.cz/feedback-options/',

    /**
     * Name of the remote folder at the root of `optionValuesUrlBase` that contains images for answers.
     *
     * **Must include a trailing slash.**
     *
     * @example `img/`
     */
    imgFolder: 'img/',

    /**
     * Resource URLs
     */
    urls: {
      therapists: 'therapists.json',
      massages: 'massages.json',
      intensityLevels: 'intensity_levels.json',
      ratings: 'ratings.json',
      massageProblems: 'massage_problems.json',
      receptionists: 'receptionists.json',
      clientSources: 'client_sources.json'
    }
  },

  /**
   * Different delays used in the app.
   */
  delay: {
    /**
     * Delay between clicking on an option and going to next question.
     */
    questionAnsweredMs: 400,

    /**
     * Delay between completing the feedback form and automatic restart of the form to let next user do it.
     *
     * If zero, automatic restart is disabled.
     */
    restartAfterCompletedMs: 60 * 1000,

    /**
     * Delay before automatic form restart when user is inactive.
     *
     * If zero, automatic restart is disabled.
     */
    restartTimoutMs: 15 * 60 * 1000,

    /**
     * After `restartTimoutMs` of inactivity a notification is shown about an upcoming restart.
     * If user does something during this time, restart is canceled.
     */
    restartCancelTimeoutMs: 5 * 1000,

    /**
     * After what time an automatically hidden notification disappears.
     */
    notificationAutoHideMs: 3 * 1000
  },

  /**
   * Constants regarding saving results through an API.
   */
  results: {
    /**
     * URL base of the API to store results with (including trailing slash).
     */
    urlBase: 'https://www.thajskemasazekiwi.cz/feedback-api/',

    /**
     * API endpoint to store results.
     */
    endpoint: 'feedback'
  },

  /**
   * Other constants
   */
  other: {
    /**
     * Bonus (discount) in CZK.
     *
     * Is offered to happy client on the thank you page if they write an online review.
     */
    reviewBonus: 100
  }
};
